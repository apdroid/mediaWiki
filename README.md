# MediaWiki Helm Chart

This is a helm chart to deploy mediawiki on kubernetes. I have tested this deployment on kubernest for Docker on MAC/Windows. This can also be deployed on preconfigured kubernetes cluster.

Directory Structre:
    1. mediawiki-thoughtworks: helm chart for mediawiki
    2. Docker: Dockerfiles for mariadb and mediawiki.
       1. mediawiki
       2. mariadb

## Installation

Clone this repo on your machine.

```bash
cd mediawiki-thoughtworks
helm install mediawiki-release.
```

## Usage

#### Application & Database Parameters

| Params          | Description                                        | Value                          |
| --------------- | -------------------------------------------------- | ------------------------------ |
| sitename        | mediaWiki default site name                        | ThoughtWorks                   |
| wikiUser        | mediaWiki admin username                           | adityapawar                    |
| image           | mediawiki docker image                             | adityapawar/mediawiki:release5 |
|                 | mariadb docker image                               | adityapawar/mariadb:release2   |
| pullPolicy      | Pull policy for docker image                       | IfNotPresent                   |
| replicas        | Replica count for mediawiki instances              | 2                              |
| strategy        | Update Strategy for pods (Rolling Update/ReCreate) | RollingUpdate                  |
| serviceType     | mediaWiki service type (Loadbalance / NodePort)    | LoadBalancer                   |
| servicePort     | mediaWiki will be available on this port           | 80                             |
| sessionAffinity | None/Client IP                                     | ClientIP                       |
| pvcName         | mediawiki dynamic pvc claim name                   | mw-pvc                         |
| pvcSize         | permanant volume claim size                        | 2Gi                            |
| host            | database host info                                 | mediawiki-mysql                |
| secret          | database password                                  | password@123                   |
| user            | database user                                      | mediawiki                      |

## Cleanup 

helm delete mediawiki-release.


