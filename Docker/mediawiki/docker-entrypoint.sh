#!/bin/bash
set -e
echo Starting wikimedia installation
php maintenance/install.php --dbname=${DATABASE_NAME} --dbserver=${DATABASE_HOST} --dbuser=${DATABASE_USER} --dbpass=${DATABASE_PASSWORD} --server="http://localhost" --scriptpath= --lang=en --pass=${WIKI_USER_PASS} ${SITE_NAME} ${WIKI_USER}
echo Wikimedia installation complete
exec "$@";